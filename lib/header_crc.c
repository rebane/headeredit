// @license MIT

#include "header.h"
#include "crc32.h"

uint32_t header_calc_crc(uint32_t start, uint32_t end, uint32_t exclude, uint8_t (*read)(uint32_t ptr)){
	uint32_t i, l, crc;
	if(end < start)return(0);
	l = end - start;
	if(l < 4)return(0);
	if((exclude != 0) && (exclude < start))return(0);
	if((exclude != 0) && (exclude > (end - 4)))return(0);
	if(read == (void *)0)read = header_read;
	crc = 0xFFFFFFFF;
	for(i = 0; i < l; i++){
		if((exclude != 0) && ((start + i) >= exclude) && ((start + i) <= (exclude + 3))){
			crc = crc32_byte(crc, 0xFF);
		}else{
			crc = crc32_byte(crc, read(start + i));
		}
	}
	return(crc);
}

uint8_t header_crc_ok(uint32_t start, uint32_t end, uint32_t header, uint8_t (*read)(uint32_t ptr)){
	uint32_t crc, l;
	crc = header_get(header, HEADER_VALUE_ID_CRC, 4, &l, read);
	if(crc == 0)return(0);
	if(l != 4)return(0);
	if(header_u32(crc, read) != header_calc_crc(start, end, crc, read))return(0);
	return(1);
}

