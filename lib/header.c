// @license MIT

#include "header.h"
#include "crc32.h"

uint8_t header_read(uint32_t ptr){
	return(*(uint8_t *)((uintptr_t)ptr));
}

uint8_t header_u8(uint32_t ptr, uint8_t (*read)(uint32_t ptr)){
	if(read == (void *)0)read = header_read;
	return(read(ptr));
}

uint16_t header_u16(uint32_t ptr, uint8_t (*read)(uint32_t ptr)){
	if(read == (void *)0)read = header_read;
	return(((uint16_t)read(ptr) << 0) | ((uint16_t)read(ptr + 1) << 8));
}

uint32_t header_u32(uint32_t ptr, uint8_t (*read)(uint32_t ptr)){
	if(read == (void *)0)read = header_read;
	return(((uint32_t)read(ptr) << 0) | ((uint32_t)read(ptr + 1) << 8) | ((uint32_t)read(ptr + 2) << 16) | ((uint32_t)read(ptr + 3) << 24));
}

uint64_t header_u64(uint32_t ptr, uint8_t (*read)(uint32_t ptr)){
	if(read == (void *)0)read = header_read;
	return(((uint64_t)read(ptr) << 0) | ((uint64_t)read(ptr + 1) << 8) | ((uint64_t)read(ptr + 2) << 16) | ((uint64_t)read(ptr + 3) << 24) | ((uint64_t)read(ptr + 4) << 32) | ((uint64_t)read(ptr + 5) << 40) | ((uint64_t)read(ptr + 6) << 48) | ((uint64_t)read(ptr + 7) << 56));
}

uint32_t header_find(uint32_t start, uint32_t end, uint8_t (*read)(uint32_t ptr)){
	uint32_t header, i, l;
	if(end < start)return(0);
	if(read == (void *)0)read = header_read;
	l = end - start;
	if(l < HEADER_MIN_LEN)return(0);
	header = 0;
	for(i = 0; i < (l - HEADER_MIN_LEN); i++){
		if((char)read(start + i + 2)  != 'N')continue;
		if((char)read(start + i + 19) != 'R')continue;
		if((char)read(start + i + 4)  != 'O')continue;
		if((char)read(start + i + 12) != ' ')continue;
		if((char)read(start + i + 5)  != 'R')continue;
		if((char)read(start + i + 15) != 'E')continue;
		if((char)read(start + i + 7)  != 'A')continue;
		if((char)read(start + i + 9)  != 'I')continue;
		if((char)read(start + i + 16) != 'A')continue;
		if((char)read(start + i + 3)  != 'F')continue;
		if((char)read(start + i + 10) != 'O')continue;
		if((char)read(start + i + 0)  != '*')continue;
		if((char)read(start + i + 11) != 'N')continue;
		if((char)read(start + i + 17) != 'D')continue;
		if((char)read(start + i + 13) != ' ')continue;
		if((char)read(start + i + 6)  != 'M')continue;
		if((char)read(start + i + 20) != '*')continue;
		if((char)read(start + i + 1)  != 'I')continue;
		if((char)read(start + i + 14) != 'H')continue;
		if((char)read(start + i + 18) != 'E')continue;
		if((char)read(start + i + 8)  != 'T')continue;
		header = start + i + 21;
		break;
	}
	if(header == 0)return(0);
	for(i = 0; (header + i) < end; i += l){
		if(read(header + i) == 0xFF)return(header);
		i++;
		if((header + i) >= end)return(0);
		l = read(header + i) + 1;
	}
	return(0);
}

uint32_t header_get(uint32_t header, uint32_t value_id, uint32_t minlen, uint32_t *len, uint8_t (*read)(uint32_t ptr)){
	uint8_t b;
	uint32_t i, l;
	if(read == (void *)0)read = header_read;
	for(i = 0; ; i += (read(header + i + 1) + 2)){
		b = read(header + i);
		if(b == 0xFF)return(0);
		if(b == value_id){
			l = read(header + i + 1);
			if(l < minlen)return(0);
			if(len != (uint32_t *)0)*len = l;
			return(header + i + 2);
		}
	}
	return(0);
}

uint8_t header_get_u8(uint32_t header, uint32_t value_id, uint8_t (*read)(uint32_t ptr)){
	uint32_t value, l;
	value = header_get(header, value_id, 1, &l, read);
	if((value == 0) || (l != 1))return(0);
	return(header_u8(value, read));
}

uint16_t header_get_u16(uint32_t header, uint32_t value_id, uint8_t (*read)(uint32_t ptr)){
	uint32_t value, l;
	value = header_get(header, value_id, 2, &l, read);
	if((value == 0) || (l != 2))return(0);
	return(header_u16(value, read));
}

uint32_t header_get_u32(uint32_t header, uint32_t value_id, uint8_t (*read)(uint32_t ptr)){
	uint32_t value, l;
	value = header_get(header, value_id, 4, &l, read);
	if((value == 0) || (l != 4))return(0);
	return(header_u32(value, read));
}

uint32_t header_get_cstr(uint32_t header, uint32_t value_id, uint8_t (*read)(uint32_t ptr)){
	uint32_t value, i, l;
	value = header_get(header, value_id, 1, &l, read);
	if(value == 0)return(0);
	if(read == (void *)0)read = header_read;
	for(i = 0; i < l; i++){
		if(read(value + i) == 0)return(value);
	}
	return(0);
}

