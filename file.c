// @license MIT

#include "file.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include "header.h"

#define FILE_NONE   0
#define FILE_EDIT   1
#define FILE_CREATE 2

#ifndef O_BINARY
#define O_BINARY 0
#endif

static int file_fd = -1, file_mode = FILE_NONE;
static uint8_t *file_buffer = NULL;
static off_t file_len;

int file_open(char *filename, uint32_t *len){
	file_fd = open(filename, O_RDWR | O_BINARY);
	if(file_fd < 0)return(-1);
	file_len = lseek(file_fd, 0, SEEK_END);
	if(file_len == (off_t)-1){
		file_close(filename);
		return(-1);
	}
	file_buffer = malloc(file_len);
	if(file_buffer == NULL){
		file_close(filename);
		return(-1);
	}
	if(lseek(file_fd, 0, SEEK_SET) < 0){
		file_close(filename);
		return(-1);
	}
	if(read(file_fd, file_buffer, file_len) != file_len){
		file_close(filename);
		return(-1);
	}
	file_mode = FILE_EDIT;
	*len = file_len;
	return(1);
}

int file_create(char *filename){
	file_fd = open(filename, O_WRONLY | O_CREAT | O_TRUNC | O_BINARY, 0644);
	if(file_fd < 0)return(-1);
	file_mode = FILE_CREATE;
	return(1);
}

void file_xor(uint8_t xor_value){
	uint32_t i;
	for(i = 0; i < file_len; i++){
		file_buffer[i] ^= xor_value;
	}
}

int file_write(const void *buf, int count){
	if(file_mode != FILE_CREATE)return(-1);
	if(write(file_fd, buf, count) != count)return(-1);
	return(count);
}

uint8_t file_read(uint32_t ptr){
	if(file_mode != FILE_EDIT)return(0);
	return(file_buffer[ptr]);
}

void *file_mem(uint32_t ptr){
	if(file_mode != FILE_EDIT)return(NULL);
	return(&file_buffer[ptr]);
}

int file_close(){
	int retval;
	retval = 1;
	if(file_mode == FILE_EDIT){
		if(file_fd >= 0){
			if(lseek(file_fd, 0, SEEK_SET) < 0){
				retval = -1;
				goto ready;
			}
			if(write(file_fd, file_buffer, file_len) != file_len){
				retval = -1;
				goto ready;
			}
		}
	}else if(file_mode == FILE_CREATE){
	}
ready:
	file_mode = FILE_NONE;
	if(file_buffer != NULL)free(file_buffer);
	file_buffer = 0;
	if(file_fd != -1)close(file_fd);
	file_fd = -1;
	return(retval);
}

